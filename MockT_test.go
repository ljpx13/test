package test

import "testing"

func TestMockTFatalfPopulatesFailureMessage(t *testing.T) {
	// Arrange.
	mockT := NewMockT()

	// Act.
	mockT.Fatalf("Hello, %v!", "World")

	// Assert.
	if mockT.failureMessage != "Hello, World!" {
		t.Fatalf("expected 'Hello, World!', actually '%v'", mockT.failureMessage)
	}
}

func TestMockTFatalfCanOnlyBeCalledOnce(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	mockT.Fatalf("Hello, %v!", "World")

	// Act.
	didPanic := func() (panicked bool) {
		panicked = false

		defer func() {
			if r := recover(); r != nil {
				panicked = true
			}
		}()

		mockT.Fatalf("Hello, %v!", "Again")
		return
	}()

	// Assert.
	if !didPanic {
		t.Fatalf("expected the call to panic")
	}
}

func TestMockTDidFailWithAlwaysReturnsFalseForEmptyString(t *testing.T) {
	// Arrange.
	mockT := NewMockT()

	// Act.
	matched := mockT.DidFailWith(``)

	// Assert.
	if matched {
		t.Fatalf("expected no matches")
	}
}

func TestMockTDidFail(t *testing.T) {
	// Arrange.
	mockT := NewMockT()

	// Act.
	failed1 := mockT.DidFail()
	mockT.Fatalf("Hello, %v!", "World")
	failed2 := mockT.DidFail()

	// Assert.
	if failed1 {
		t.Fatalf("reported true for didFail when hadn't failed")
		return
	}

	if !failed2 {
		t.Fatalf("did not report true for didFail when had failed")
	}
}

func TestMockTDidFailWithSuccess(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	mockT.Fatalf("expected 3, actually 4")

	// Act.
	matched := mockT.DidFailWith(`^expected \d, actually \d$`)

	// Assert.
	if !matched {
		t.Fatalf("expected a match")
	}
}

func TestMockTGetFailureMessage(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	mockT.Fatalf("Hello, %v!", "World")

	// Act.
	failureMessage := mockT.GetFailureMessage()

	// Assert.
	if failureMessage != "Hello, World!" {
		t.Fatalf("expected 'Hello, World!', actual '%v'", failureMessage)
	}
}

func TestMockTStubs(t *testing.T) {
	// Arrange.
	mockT := NewMockT()

	// Act.
	mockT.Helper()
	name := mockT.Name()

	// Assert.
	if name != "" || mockT.failureMessage != "" {
		t.Fatalf("expected stub calls to do nothing")
	}
}
