package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// TrueAssertion is an assertion that evaluates if the subject is true.
type TrueAssertion struct {
	y interface{}
}

var _ test.Assertion = &TrueAssertion{}

// True returns an TrueAssertion.
func True() *TrueAssertion {
	return &TrueAssertion{}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *TrueAssertion) EvaluateFor(x interface{}) *test.Result {
	val, ok := x.(bool)
	passed := ok && val

	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
	}

	detail := "x is true"
	if !passed {
		detail = "x is not a boolean or is not true"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
