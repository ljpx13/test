package is

import (
	"testing"
)

func TestTrue(t *testing.T) {
	testCases := []struct {
		x interface{}
		e bool
	}{
		{x: 1 == 1, e: true},
		{x: 1 == 2, e: false},
		{x: true, e: true},
		{x: false, e: false},
		{x: "asd" == "asd", e: true},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := True().EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v == true to be %v, but was %v", testCase.x, testCase.e, actual)
		}
	}
}
