package is

import (
	"io"
	"testing"
)

func TestNotNil(t *testing.T) {
	testCases := []struct {
		x interface{}
		e bool
	}{
		{x: 5, e: true},
		{x: "Hello", e: true},
		{x: nil, e: false},
		{x: (*io.Writer)(nil), e: false},
		{x: getNilTestMapAlias(), e: false},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := NotNil().EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v != nil to be %v, but was %v", testCase.x, testCase.e, actual)
		}
	}
}
