package is

import (
	"io"
	"testing"
)

func TestNil(t *testing.T) {
	testCases := []struct {
		x interface{}
		e bool
	}{
		{x: 5, e: false},
		{x: "Hello", e: false},
		{x: nil, e: true},
		{x: (*io.Writer)(nil), e: true},
		{x: getNilTestMapAlias(), e: true},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := Nil().EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v == nil to be %v, but was %v", testCase.x, testCase.e, actual)
		}
	}
}

type nilTestMapAlias map[string]string

func getNilTestMapAlias() nilTestMapAlias {
	return nil
}
