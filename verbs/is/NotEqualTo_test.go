package is

import (
	"io"
	"testing"
)

func TestNotEqualTo(t *testing.T) {
	testCases := []struct {
		x interface{}
		y interface{}
		e bool
	}{
		{x: 5, y: 6, e: true},
		{x: 5, y: 5, e: false},
		{x: 5, y: 6.0, e: true},
		{x: 5, y: 5.0, e: true},
		{x: 5, y: "Hello", e: true},
		{x: "Hello", y: "Hello", e: false},
		{x: "Hello", y: "hello", e: true},
		{x: "Hello", y: nil, e: true},
		{x: nil, y: nil, e: false},
		{x: (*io.Writer)(nil), y: (*io.Reader)(nil), e: true},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := NotEqualTo(testCase.y).EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v != %v to be %v, but was %v", testCase.x, testCase.y, testCase.e, actual)
		}
	}
}
