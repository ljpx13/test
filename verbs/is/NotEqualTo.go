package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// NotEqualToAssertion is an assertion that evaluates if the subject and the
// single additional parameter are not equal to each other.
type NotEqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &NotEqualToAssertion{}

// NotEqualTo returns an NotEqualToAssertion for the provided comperand y.
func NotEqualTo(y interface{}) *NotEqualToAssertion {
	return &NotEqualToAssertion{
		y: y,
	}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *NotEqualToAssertion) EvaluateFor(x interface{}) *test.Result {
	passed := x != a.y
	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
		"y": fmt.Sprintf("(%v) %v", reflect.TypeOf(a.y), a.y),
	}

	detail := "x is not equal to y"
	if !passed {
		detail = "x is equal to y"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
