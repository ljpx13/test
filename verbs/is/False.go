package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// FalseAssertion is an assertion that evaluates if the subject is false.
type FalseAssertion struct {
	y interface{}
}

var _ test.Assertion = &FalseAssertion{}

// False returns an FalseAssertion.
func False() *FalseAssertion {
	return &FalseAssertion{}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *FalseAssertion) EvaluateFor(x interface{}) *test.Result {
	val, ok := x.(bool)
	passed := ok && !val

	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
	}

	detail := "x is false"
	if !passed {
		detail = "x is not a boolean or is not false"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
