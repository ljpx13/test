package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// EqualToAssertion is an assertion that evaluates if the subject and the
// single additional parameter are equal to each other.
type EqualToAssertion struct {
	y interface{}
}

var _ test.Assertion = &EqualToAssertion{}

// EqualTo returns an EqualToAssertion for the provided comperand y.
func EqualTo(y interface{}) *EqualToAssertion {
	return &EqualToAssertion{
		y: y,
	}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *EqualToAssertion) EvaluateFor(x interface{}) *test.Result {
	passed := x == a.y
	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
		"y": fmt.Sprintf("(%v) %v", reflect.TypeOf(a.y), a.y),
	}

	detail := "x is equal to y"
	if !passed {
		detail = "x is not equal to y"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
