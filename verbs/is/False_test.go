package is

import (
	"testing"
)

func TestFalse(t *testing.T) {
	testCases := []struct {
		x interface{}
		e bool
	}{
		{x: 1 == 1, e: false},
		{x: 1 == 2, e: true},
		{x: true, e: false},
		{x: false, e: true},
		{x: "asd" == "asd", e: false},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := False().EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v == true to be %v, but was %v", testCase.x, testCase.e, actual)
		}
	}
}
