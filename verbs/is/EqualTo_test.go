package is

import (
	"io"
	"testing"
)

func TestEqualTo(t *testing.T) {
	testCases := []struct {
		x interface{}
		y interface{}
		e bool
	}{
		{x: 5, y: 6, e: false},
		{x: 5, y: 5, e: true},
		{x: 5, y: 6.0, e: false},
		{x: 5, y: 5.0, e: false},
		{x: 5, y: "Hello", e: false},
		{x: "Hello", y: "Hello", e: true},
		{x: "Hello", y: "hello", e: false},
		{x: "Hello", y: nil, e: false},
		{x: nil, y: nil, e: true},
		{x: (*io.Writer)(nil), y: (*io.Reader)(nil), e: false},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := EqualTo(testCase.y).EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected %v == %v to be %v, but was %v", testCase.x, testCase.y, testCase.e, actual)
		}
	}
}
