package is

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// NotNilAssertion is an assertion that evaluates if the subject is not nil.
type NotNilAssertion struct {
	y interface{}
}

var _ test.Assertion = &NotNilAssertion{}

// NotNil returns an NotNilAssertion.
func NotNil() *NotNilAssertion {
	return &NotNilAssertion{}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *NotNilAssertion) EvaluateFor(x interface{}) *test.Result {
	passed := x != nil
	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
	}

	detail := "x is nil"
	if passed {
		v := reflect.ValueOf(x)
		passed = !v.IsValid() || (v.Kind() != reflect.Ptr && v.Kind() != reflect.Map) || !v.IsNil()
	}
	if passed {
		detail = "x is not nil"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
