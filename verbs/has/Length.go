package has

import (
	"fmt"
	"reflect"

	"gitlab.com/ljpx13/test"
)

// LengthAssertion is an assertion that evaluates if the subjects length is
// equal to the provided value.
type LengthAssertion struct {
	y int
}

var _ test.Assertion = &LengthAssertion{}

// Length returns an LengthAssertion for the provided expected length y.
func Length(y int) *LengthAssertion {
	return &LengthAssertion{
		y: y,
	}
}

// EvaluateFor evalutes the assertion against the subject, x.
func (a *LengthAssertion) EvaluateFor(x interface{}) *test.Result {
	xv := reflect.ValueOf(x)
	xvk := xv.Kind()

	_, isValidKind := map[reflect.Kind]struct{}{
		reflect.Array:  struct{}{},
		reflect.Chan:   struct{}{},
		reflect.Map:    struct{}{},
		reflect.Slice:  struct{}{},
		reflect.String: struct{}{},
	}[xvk]

	passed := isValidKind && xv.Len() == a.y

	parameterSummary := map[string]string{
		"x": fmt.Sprintf("(%v) %v", reflect.TypeOf(x), x),
		"y": fmt.Sprintf("(%v) %v", reflect.TypeOf(a.y), a.y),
	}

	if isValidKind {
		lenx := xv.Len()
		parameterSummary["len(x)"] = fmt.Sprintf("(%v) %v", reflect.TypeOf(lenx), lenx)
	}

	detail := "the length of x is y"
	if !passed {
		detail = "the length of x is not y or x does not have a length"
	}

	return &test.Result{
		Passed:           passed,
		ParameterSummary: parameterSummary,
		Detail:           detail,
	}
}
