package has

import (
	"testing"
)

func TestLength(t *testing.T) {
	testCases := []struct {
		x interface{}
		y int
		e bool
	}{
		{x: []byte{1, 2}, y: 2, e: true},
		{x: []byte{1, 2}, y: 1, e: false},
		{x: []byte{1, 2}, y: 3, e: false},
		{x: map[int]int{0: 1, 1: 2}, y: 2, e: true},
		{x: map[int]int{0: 1, 1: 2}, y: 1, e: false},
		{x: map[int]int{0: 1, 1: 2}, y: 3, e: false},
		{x: "ad", y: 2, e: true},
		{x: "ad", y: 1, e: false},
		{x: "ad", y: 3, e: false},
		{x: [2]byte{1, 2}, y: 2, e: true},
		{x: [2]byte{1, 2}, y: 1, e: false},
		{x: [2]byte{1, 2}, y: 3, e: false},
		{x: 2, y: 2, e: false},
		{x: 2, y: 1, e: false},
		{x: 2, y: 3, e: false},
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		actual := Length(testCase.y).EvaluateFor(testCase.x).Passed

		// Assert.
		if actual != testCase.e {
			t.Fatalf("expected len(%v) == %v to be %v, but was %v", testCase.x, testCase.y, testCase.e, actual)
		}
	}
}
