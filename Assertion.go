package test

// Assertion represents a single assertable fact.  Assertions are provided as
// the last parameter to the test.That function.  The type *is.Equal is an
// example of an Assertion.
type Assertion interface {
	EvaluateFor(x interface{}) *Result
}
