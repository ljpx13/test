package test

// That is the core function of this package.  It accepts a TLike (usually a
// *testing.T), a subject, x, and an assertion.
func That(t TLike, x interface{}, assertion Assertion) {
	t.Helper()
	assertion.EvaluateFor(x).AssertPassed(t)
}
