package test

import "fmt"

// Result represents the result of evaluating an Assertion.  It provides
// information used for error reporting.
type Result struct {
	Passed           bool
	ParameterSummary map[string]string
	Detail           string
}

// AssertPassed will fail the provided TLike with a user-friendly message if the
// result indicates failure.
func (r *Result) AssertPassed(t TLike) {
	t.Helper()

	if r.Passed {
		return
	}

	failureMessage := fmt.Sprintf("\n\n× %v\n", t.Name())
	for parameterName, typeName := range r.ParameterSummary {
		failureMessage += fmt.Sprintf("%v: %v\n", parameterName, typeName)
	}

	failureMessage += fmt.Sprintf("\n%v\n\n", r.Detail)
	t.Fatalf(failureMessage)
}
