package test

import "testing"

// TLike is an interface that defines the methods of a *testing.T that are used
// by this package.  Having this interface allows the assertions in this package
// to be unit tested as well as function in external unit tests.
type TLike interface {
	Fatalf(format string, args ...interface{})
	Helper()
	Name() string
}

var _ TLike = &testing.T{}
