package test

import (
	"strings"
	"testing"
)

func TestResultAssertPassedSuccess(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	result := &Result{
		Passed: true,
	}

	// Act.
	result.AssertPassed(mockT)

	// Assert.
	if mockT.DidFail() {
		t.Fatalf("was not expecting failure, but failed with '%v'", mockT.GetFailureMessage())
	}
}

func TestResultAssertPassedFailure(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	result := &Result{
		Passed: false,
		ParameterSummary: map[string]string{
			"x": "int",
			"y": "int",
			"s": "string",
		},
		Detail: "x + y cannot equal a string",
	}

	// Act.
	result.AssertPassed(mockT)

	// Assert.
	actual := mockT.GetFailureMessage()
	if !strings.Contains(actual, "\n\nx + y cannot equal a string\n\n") {
		t.Fatalf("failure string did not match: %v", actual)
	}
}
