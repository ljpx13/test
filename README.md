![](./icon.png)

# test

Package `test` provides a thin assertion wrapper around the basic Go testing
package.

## Usage Example

```go
func TestAddCanSum(t *testing.T) {
    // Arrange.
    expected := 5

    // Act.
    actual := Add(2, 3)

    // Assert.
    test.That(t, actual, is.EqualTo(expected))
}
```
