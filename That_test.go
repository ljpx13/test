package test

import (
	"testing"
)

func TestThatSuccess(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	v1 := 2
	v2 := 2

	// Act.
	That(mockT, v1, &basicEqualAssertionForTesting{y: v2})

	// Assert.
	if mockT.DidFail() {
		t.Fatalf("expected no failure, but failed with '%v'", mockT.GetFailureMessage())
	}
}

func TestThatFailure(t *testing.T) {
	// Arrange.
	mockT := NewMockT()
	v1 := 2
	v2 := 3

	// Act.
	That(mockT, v1, &basicEqualAssertionForTesting{y: v2})

	// Assert.
	if !mockT.DidFail() {
		t.Fatalf("expected a failure, but did not fail")
	}
}

// -----------------------------------------------------------------------------

type basicEqualAssertionForTesting struct {
	y interface{}
}

var _ Assertion = &basicEqualAssertionForTesting{}

func (a *basicEqualAssertionForTesting) EvaluateFor(x interface{}) *Result {
	return &Result{
		Passed: x == a.y,
	}
}
